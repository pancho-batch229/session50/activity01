import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Login() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	});

	function loginUser(event) {
		event.preventDefault();

		setEmail('');
		setPassword('');

		alert('You are now logged in! Welcome!');
	}

	return(
		<Form onSubmit={(event) => loginUser(event)}>
			{/*User email*/}
		     <Form.Group className="mb-3" controlId="userEmail">
		       <Form.Label>Email address</Form.Label>
		       <Form.Control type="email" placeholder="Enter email" value={email} onChange={event => setEmail(event.target.value)} required/>

		       <Form.Text className="text-muted">
		         We'll never share your email with anyone else.
		       </Form.Text>
		     </Form.Group>

		     {/*user password*/}
		     <Form.Group className="mb-3" controlId="formBasicPassword">
		       <Form.Label>Password</Form.Label>
		       <Form.Control type="password1" placeholder="Password"  value={password} onChange={event => setPassword(event.target.value)} required/>
		     </Form.Group>

		     {
		     	(isActive) ?
			     <Button variant="primary" type="submit" controlId="submitBtn">
			       Login
			     </Button>
			    :

			     <Button variant="primary" type="submit" controlId="submitBtn" disabled>
			       Login
			     </Button>		    
		     }
		   </Form>
	);
}