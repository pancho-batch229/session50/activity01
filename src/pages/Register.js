import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Register() {
	
	// state hooks - store values of the input fields
	// getter setter
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	
	const [isActive, setIsActive] = useState(false);

	console.log(email, password1, password2);

	useEffect(() => {
		// validation to enable register buttton when all fields are populated and both passwords match

		if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	});

	// function to simulate  user registration

	function registerUser(event) {
		// preventvents page redirection via form submission
		event.preventDefault();

		setEmail('');
		setPassword1('');
		setPassword2('');

		alert("Thank you for registering!");
	}

	return (
		<Form onSubmit={(event) => registerUser(event)}>
		     <Form.Group className="mb-3" controlId="userEmail">
		       <Form.Label>Email address</Form.Label>
		       <Form.Control type="email" placeholder="Enter email" value={email} onChange={event => setEmail(event.target.value)} required/>
		       {/*email or e (shortcut)*/}
		       <Form.Text className="text-muted">
		         We'll never share your email with anyone else.
		       </Form.Text>
		     </Form.Group>

		     <Form.Group className="mb-3" controlId="formBasicPassword">
		       <Form.Label>Password</Form.Label>
		       <Form.Control type="password1" placeholder="Password"  value={password1} onChange={event => setPassword1(event.target.value)} required/>
		     </Form.Group>

		     <Form.Group className="mb-3" controlId="formBasicPassword">
		       <Form.Label>Verify Password</Form.Label>
		       <Form.Control type="password2" placeholder=" Verify Password"   value={password2} onChange={event => setPassword2(event.target.value)} required/>
		     </Form.Group>	
		     {/*conditional rendering - if active, button is clickable - if inactive it is not clickable*/}
		     {
		     	(isActive) ?
			     <Button variant="primary" type="submit" controlId="submitBtn">
			       Register
			     </Button>
			    :

			     <Button variant="primary" type="submit" controlId="submitBtn" disabled>
			       Register
			     </Button>		    
		     }

		   </Form>
	);
}