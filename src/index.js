import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.js';
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
  <App />
  </React.StrictMode>
);

// const name = "John Smith";
// // embedding
// const user = {
//   firstName: 'Razputin',
//   lastName: 'Aquato'
// }

// function formatName(user) {
//   return user.firstName + ' ' + user.lastName;
// }

// ReactDOM.render(element, document.getElementById('root'));