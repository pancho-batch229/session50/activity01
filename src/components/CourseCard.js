import {Card, Button} from 'react-bootstrap';
import {useState} from 'react';
//  {} destructors

export default function CourseCard({courseProps}) {
	// checks if data is passed

	console.log(courseProps);
	console.log(typeof courseProps);

	const {name, description, price} = courseProps;
//  {} destructors no need for dot notations
	/*
		3 hooks in react
		1. useState
		2. useEffect
		3. useContent

		useStates hook for components able to store states
		states are used to keep track  of info to individual components
						original  set new values
		syntax -> const [getter, setter] = useState(initialGetterValue);

	*/

	const [count, setCount] = useState(0);

	function enroll() {
		setCount(count + 1);
		console.log("Enrollees " + count);
	}

	return(
		<div>
			{/*<Card className="cardHighlight p-3">
				<Card.Title>
					<h4 className="text-center">Sample Course</h4>
				</Card.Title>
				<Card.Body>
					<Card.Text>
						<p className="m-0">Description:</p>
						<p className="mb-3">This is a sample course offering.</p>
						<p className="m-0">Price:</p>
						<h3 className="m-0">PHP 40,000</h3>
					</Card.Text>
					<Button className="primary">Enroll</Button>
				</Card.Body>
			</Card>*/}
			<Card>
		        <Card.Body>
		            <Card.Title>{name}</Card.Title>
		            <Card.Subtitle>Description:</Card.Subtitle>
		            <Card.Text>{description}</Card.Text>
		            <Card.Subtitle>Price:</Card.Subtitle>
		            <Card.Text>{price}</Card.Text>
		            <Card.Text>Enrollees: {count}</Card.Text>
		            <Button onClick={enroll} variant="primary">Enroll</Button>
		        </Card.Body>
		    </Card>
		</div>
	)
}