import { Link } from 'react-router-dom';

export default function UnknownPage() {
	return (
		<div>
			<h1>Page Does Not Exist</h1>
			<Link to="/">Go back? </Link>
		</div>
	);
}