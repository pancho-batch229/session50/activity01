import courseData from '../data/courses.js';
import CourseCard from '../components/CourseCard.js'

export default function Courses() {

	console.log(courseData);
	// The "course" in the CourseCard component is called a "prop" which is a shorthand for "property" since components are considered as objects in React JS
    // The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions rather than hard coded values which use double quotes ("")
	// We can pass information from one component to another using props. This is referred to as "props drilling"
	
	// The "map" method loops through the individual course objects in our array and returns a component for each course
    // Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed, added or removed
    // Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our coursesData array using the courseProp
    // const courses = courses

    const courses = courseData.map(course => {
    	return (
		<>
			<CourseCard key={courseData.id} courseProps={course}/>
		</>
		)
    });

	return (
		<>
			<h1>Courses</h1> 
			{courses}
		</>

	)
}